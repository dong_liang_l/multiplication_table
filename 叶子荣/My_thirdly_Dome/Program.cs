﻿using System;

namespace My_thirdly_Dome
{
    class Program
    {
        static void Main(string[] args)
        {
            for( int i = 1; i<10; i++)
            {
                for( int j = 1; j <= i; j++)
                {
                    Console.Write("{0} X {1} = {2} \t",j,i,j*i );
                }
                Console.Write("\n"); 
            }
        }
    }
}

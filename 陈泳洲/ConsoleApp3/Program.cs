﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 1; i <= 9; i++)
            {
                for (int k = 1; k <= i; k++)
                {
                    Console.Write("{0}*{1}={2}\t", i, k, i * k);

                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}
